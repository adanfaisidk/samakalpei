package sn.isi.m2gl.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.isi.m2gl.web.rest.TestUtil;

public class RetraitTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Retrait.class);
        Retrait retrait1 = new Retrait();
        retrait1.setId(1L);
        Retrait retrait2 = new Retrait();
        retrait2.setId(retrait1.getId());
        assertThat(retrait1).isEqualTo(retrait2);
        retrait2.setId(2L);
        assertThat(retrait1).isNotEqualTo(retrait2);
        retrait1.setId(null);
        assertThat(retrait1).isNotEqualTo(retrait2);
    }
}
