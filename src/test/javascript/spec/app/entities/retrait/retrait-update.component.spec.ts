import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { SunuKalpeTestModule } from '../../../test.module';
import { RetraitUpdateComponent } from 'app/entities/retrait/retrait-update.component';
import { RetraitService } from 'app/entities/retrait/retrait.service';
import { Retrait } from 'app/shared/model/retrait.model';

describe('Component Tests', () => {
  describe('Retrait Management Update Component', () => {
    let comp: RetraitUpdateComponent;
    let fixture: ComponentFixture<RetraitUpdateComponent>;
    let service: RetraitService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SunuKalpeTestModule],
        declarations: [RetraitUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(RetraitUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RetraitUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RetraitService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Retrait(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Retrait();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
