import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SunuKalpeTestModule } from '../../../test.module';
import { RetraitDetailComponent } from 'app/entities/retrait/retrait-detail.component';
import { Retrait } from 'app/shared/model/retrait.model';

describe('Component Tests', () => {
  describe('Retrait Management Detail Component', () => {
    let comp: RetraitDetailComponent;
    let fixture: ComponentFixture<RetraitDetailComponent>;
    const route = ({ data: of({ retrait: new Retrait(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SunuKalpeTestModule],
        declarations: [RetraitDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(RetraitDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RetraitDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load retrait on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.retrait).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
