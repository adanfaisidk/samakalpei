import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { RetraitService } from 'app/entities/retrait/retrait.service';
import { IRetrait, Retrait } from 'app/shared/model/retrait.model';

describe('Service Tests', () => {
  describe('Retrait Service', () => {
    let injector: TestBed;
    let service: RetraitService;
    let httpMock: HttpTestingController;
    let elemDefault: IRetrait;
    let expectedResult: IRetrait | IRetrait[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(RetraitService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Retrait(0, currentDate, 0);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateRetrait: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Retrait', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateRetrait: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateRetrait: currentDate,
          },
          returnedFromService
        );

        service.create(new Retrait()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Retrait', () => {
        const returnedFromService = Object.assign(
          {
            dateRetrait: currentDate.format(DATE_FORMAT),
            montantRetrait: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateRetrait: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Retrait', () => {
        const returnedFromService = Object.assign(
          {
            dateRetrait: currentDate.format(DATE_FORMAT),
            montantRetrait: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateRetrait: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Retrait', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
