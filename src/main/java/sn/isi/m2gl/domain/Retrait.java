package sn.isi.m2gl.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Retrait.
 */
@Entity
@Table(name = "retrait")
public class Retrait implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date_retrait")
    private LocalDate dateRetrait;

    @Column(name = "montant_retrait")
    private Double montantRetrait;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateRetrait() {
        return dateRetrait;
    }

    public Retrait dateRetrait(LocalDate dateRetrait) {
        this.dateRetrait = dateRetrait;
        return this;
    }

    public void setDateRetrait(LocalDate dateRetrait) {
        this.dateRetrait = dateRetrait;
    }

    public Double getMontantRetrait() {
        return montantRetrait;
    }

    public Retrait montantRetrait(Double montantRetrait) {
        this.montantRetrait = montantRetrait;
        return this;
    }

    public void setMontantRetrait(Double montantRetrait) {
        this.montantRetrait = montantRetrait;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Retrait)) {
            return false;
        }
        return id != null && id.equals(((Retrait) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Retrait{" +
            "id=" + getId() +
            ", dateRetrait='" + getDateRetrait() + "'" +
            ", montantRetrait=" + getMontantRetrait() +
            "}";
    }
}
