package sn.isi.m2gl.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
@Entity
@Table(name = "compte")
public class Compte implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "libelle")
    private String libelle;
    @Column(name = "solde_veille")
    private Double solde_veille;
    @Column(name = "numero_compte")
    private String numero_compte;
    @Column(name = "pourcentage")
    private Float pourcentage;
    @Column(name = "balance")
    private Double balance;
    @Column(name = "type_compte")
    private String type_compte;
    @Column(name = "date_ouverture")
    private Date date_ouverture= new Date();
    @Column(name = "blocage_compte")
    private Date blocage_compte;
    @Column(name = "solde_plafond")
    private Double solde_plafond;

    public Compte(int i, String epargne, int i1, String n0256, double v, int i2, String compte_epargne, String s, String s1, int i3) {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Double getSolde_veille() {
        return solde_veille;
    }

    public void setSolde_veille(Double solde_veille) {
        this.solde_veille = solde_veille;
    }

    public String getNumero_compte() {
        return numero_compte;
    }

    public void setNumero_compte(String numero_compte) {
        this.numero_compte = numero_compte;
    }

    public Float getPourcentage() {
        return pourcentage;
    }

    public void setPourcentage(Float pourcentage) {
        this.pourcentage = pourcentage;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getType_compte() {
        return type_compte;
    }

    public void setType_compte(String type_compte) {
        this.type_compte = type_compte;
    }

    public Date getDate_ouverture() {
        return date_ouverture;
    }

    public void setDate_ouverture(Date date_ouverture) {
        this.date_ouverture = date_ouverture;
    }

    public Date getBlocage_compte() {
        return blocage_compte;
    }

    public void setBlocage_compte(Date blocage_compte) {
        this.blocage_compte = blocage_compte;
    }

    public Double getSolde_plafond() {
        return solde_plafond;
    }

    public void setSolde_plafond(Double solde_plafond) {
        this.solde_plafond = solde_plafond;
    }


    public Compte(Long id, String libelle, Double solde_veille, String numero_compte, Float pourcentage, Double balance, String type_compte, Date date_ouverture, Date blocage_compte, Double solde_plafond) {
        this.id = id;
        this.libelle = libelle;
        this.solde_veille = solde_veille;
        this.numero_compte = numero_compte;
        this.pourcentage = pourcentage;
        this.balance = balance;
        this.type_compte = type_compte;
        this.date_ouverture = date_ouverture;
        this.blocage_compte = blocage_compte;
        this.solde_plafond = solde_plafond;
    }
}
