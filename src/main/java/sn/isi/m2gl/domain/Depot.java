package sn.isi.m2gl.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Depot.
 */
@Entity
@Table(name = "depot")
public class Depot implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date_depot")
    private LocalDate dateDepot;

    @Column(name = "montant_depot")
    private Double montantDepot;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateDepot() {
        return dateDepot;
    }

    public Depot dateDepot(LocalDate dateDepot) {
        this.dateDepot = dateDepot;
        return this;
    }

    public void setDateDepot(LocalDate dateDepot) {
        this.dateDepot = dateDepot;
    }

    public Double getMontantDepot() {
        return montantDepot;
    }

    public Depot montantDepot(Double montantDepot) {
        this.montantDepot = montantDepot;
        return this;
    }

    public void setMontantDepot(Double montantDepot) {
        this.montantDepot = montantDepot;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Depot)) {
            return false;
        }
        return id != null && id.equals(((Depot) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Depot{" +
            "id=" + getId() +
            ", dateDepot='" + getDateDepot() + "'" +
            ", montantDepot=" + getMontantDepot() +
            "}";
    }
}
