package sn.isi.m2gl.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

/**
 * A Transaction.
 */
@Entity
@Table(name = "transaction")
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date")
    private Date date = new Date();

    @Column(name = "numer")
    private String numer;

    @Column(name = "reference")
    private String reference;

    @Column(name = "type")
    private String type;

    @Column(name = "frais")
    private Double frais;

    @Column(name = "montant")
    private Double montant;

    @Column(name = "statut")
    private Boolean statut;

    @ManyToOne
    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
    private Compte compteTo;

    @ManyToOne
    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
    private Compte compteFrom;

    public Transaction(Long id, Date date, String numer, String reference, String type, Double frais, Double montant, Boolean statut, Compte compteTo, Compte compteFrom) {
        this.id = id;
        this.date = date;
        this.numer = numer;
        this.reference = reference;
        this.type = type;
        this.frais = frais;
        this.montant = montant;
        this.statut = statut;
        this.compteTo = compteTo;
        this.compteFrom = compteFrom;
    }

    public Transaction(int i, String s, String ref001, String n002, String retrait, double frais, int i1, boolean statut, Compte compteTo, Compte comptefrom) {
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getStatut() {
        return statut;
    }

    public String getNumer() {
        return numer;
    }

    public Transaction numer(String numer) {
        this.numer = numer;
        return this;
    }

    public void setNumer(String numer) {
        this.numer = numer;
    }

    public String getReference() {
        return reference;
    }

    public Transaction reference(String reference) {
        this.reference = reference;
        return this;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getType() {
        return type;
    }

    public Transaction type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getFrais() {
        return frais;
    }

    public Transaction frais(Double frais) {
        this.frais = frais;
        return this;
    }

    public void setFrais(Double frais) {
        this.frais = frais;
    }

    public Double getMontant() {
        return montant;
    }

    public Transaction montant(Double montant) {
        this.montant = montant;
        return this;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Boolean isStatut() {
        return statut;
    }

    public Transaction statut(Boolean statut) {
        this.statut = statut;
        return this;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Compte getCompteTo() {
        return compteTo;
    }

    public Transaction compteTo(Compte compte) {
        this.compteTo = compte;
        return this;
    }

    public void setCompteTo(Compte compte) {
        this.compteTo = compte;
    }

    public Compte getCompteFrom() {
        return compteFrom;
    }

    public Transaction compteFrom(Compte compte) {
        this.compteFrom = compte;
        return this;
    }

    public void setCompteFrom(Compte compte) {
        this.compteFrom = compte;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Transaction)) {
            return false;
        }
        return id != null && id.equals(((Transaction) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Transaction{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", numer='" + getNumer() + "'" +
            ", reference='" + getReference() + "'" +
            ", type='" + getType() + "'" +
            ", frais=" + getFrais() +
            ", montant=" + getMontant() +
            ", statut='" + isStatut() + "'" +
            "}";
    }
}
