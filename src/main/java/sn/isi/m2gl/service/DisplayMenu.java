package sn.isi.m2gl.service;

import sn.isi.m2gl.domain.Compte;
import sn.isi.m2gl.domain.Transaction;
import sn.isi.m2gl.repository.CompteRepository;
import sn.isi.m2gl.repository.TransactionRepository;

import java.util.Scanner;

public class DisplayMenu {
    /**
     * MENU PRINCIPAL
     */
    public void showWelcomeMenu() {

        boolean arret = false;
        while (!arret) {

            System.out.println("Bienvenue sur la platforme de gestion de Prortefeuil Elecctronique!!");
            System.out.println("1-Admin");
            System.out.println("2-Afficher Les Differents types de comptes");
            System.out.println("3-Afficher Les transactions de types retrait");
            System.out.println("4-Afficher Les transactions de types depot");
            System.out.println("Veuillez choisir un profil pour continuer:");
            Scanner scanner = new Scanner(System.in);
            int choice = scanner.nextInt();
            //clearConsole();
            switch (choice) {
                case 1:
                    showAdminMenu();
                    break;
                case 2:
                    //Menu Comptes
                    break;
                case 3:
                    // Menu Transactions retrait
                    break;
                case 4:
                    // Menu Transactions retrait
                    break;
                case 5:
                    arret = true;
                    break;

                default:
                    System.out.println("entrez un choix entre 1 et 3");
                    break;

            }

        }

    }

    /**
     * Menu Admin
     */
    public void showAdminMenu() {
        int choice;
        CompteRepository compteRepository  = new CompteRepository();
        TransactionRepository transactionRepository = new TransactionRepository();
        Compte[] comptes = compteRepository.getAllCompte();

        do {
            System.out.println("Bienvenue Admin!!");
            System.out.println("1-Afficher les Transactions");
            System.out.println("2-Afficher les Comptes");
            System.out.println("3-Faire une Une operation de depot");
            System.out.println("4-Faire une Une operation de retrait");
            System.out.println("5-Quitter");
            System.out.println("Veuillez choisir une option pour continuer:");
            Scanner scanner = new Scanner(System.in);
            choice = scanner.nextInt();
            //clearConsole();
            switch (choice) {
                case 1:
                    System.out.println(":Nos Comptes");
                    for (int i = 0; i < comptes.length; i++) {
                        Compte compte  = comptes[i];
                        System.out.println(String.format("> %S %S", compte.getId(), compte.getLibelle()));
                    }
                    System.out.println("Choissisez un compte et voir ces transactions:");
                    int idCompteTo = scanner.nextInt();
                    int idCompteFrom = scanner.nextInt();
                    if (idCompteTo == 1 && idCompteFrom == 2) {
                        System.out.println("Lestransactions du 05/05/2020:");
                        Transaction[] transactions = transactionRepository.getAll();
                        for (int i = 0; i < transactions.length; i++) {
                            Transaction transaction = transactions[i];
                            System.out.println(String.format("> %S %S", transaction.getReference(), transaction.getNumer()));
                        }
                    }
                    break;
                case 2:
                    //
                    break;
                case 3:
                    //
                    break;

                default:
                    System.out.println("entrez un choix entre 1 et 5");
                    break;

            }

        } while (choice != 5);

    }


    public final static void clearConsole() {
        try {
            final String os = System.getProperty("os.name");
            if (os.contains("Windows")) {
                Runtime.getRuntime().exec("cls");
            } else {
                Runtime.getRuntime().exec("clear");
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }
}
