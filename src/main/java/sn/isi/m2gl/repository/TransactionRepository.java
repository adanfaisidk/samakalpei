package sn.isi.m2gl.repository;

import org.springframework.data.domain.Example;
import sn.isi.m2gl.domain.Compte;
import sn.isi.m2gl.domain.Transaction;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.isi.m2gl.domain.User;

import java.util.Date;
import java.util.List;

/**
 * Spring Data  repository for the Transaction entity.
 */
@SuppressWarnings("unused")
public class TransactionRepository  {
    public Transaction[] getAll(){
        User user = new User();
        Compte  compteTo = new Compte(1,  "epargne", 250000,  "N0256", 20.02, 895000,  "compte epargne", "05/05/2020", "05/05/2030", 4050000);
        Compte  compteTo1 = new Compte(2,  "epargne", 69000,  "N0126", 10.02, 25699,  "compte epargne", "05/05/2020", "05/05/2030", 850000);
        Compte  compteFrom1 = new Compte(3,  "courant", 1250000,  "N963", 12.2, 56966,  "compte courant", "05/05/2030", "05/05/2025", 2922);
        Compte  compteFrom = new Compte(4,  "courant", 69300,  "N8896", 25.3, 895000,  "compte courant", "05/05/2020", "05/05/2030", 525660);

        return new Transaction[]{
            new Transaction(1,"05/05/2020","ref001","n002","retrait",260.0,45000,true,compteTo,compteFrom),
            new Transaction(2,"05/05/2020","ref025","n02566","depot",260.0,69500,true,compteTo1,compteFrom1)
        };
    }


}
