package sn.isi.m2gl.repository;

import sn.isi.m2gl.domain.Compte;
import sn.isi.m2gl.domain.Transaction;
import sn.isi.m2gl.domain.User;

public class CompteRepository {
    public Compte[] getAllCompte() {

        return new Compte[]{
            new Compte(3, "courant", 1250000, "N963", 12.2, 56966, "compte courant", "05/05/2030", "05/05/2025", 2922),
            new Compte(4, "courant", 69300, "N8896", 25.3, 895000, "compte courant", "05/05/2020", "05/05/2030", 525660)

        };
    }
}
