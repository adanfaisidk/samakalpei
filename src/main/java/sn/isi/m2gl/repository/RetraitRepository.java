package sn.isi.m2gl.repository;

import sn.isi.m2gl.domain.Retrait;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Retrait entity.
 */
@SuppressWarnings("unused")
public class RetraitRepository  {
}
