import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { SunuKalpeSharedModule } from 'app/shared/shared.module';
import { SunuKalpeCoreModule } from 'app/core/core.module';
import { SunuKalpeAppRoutingModule } from './app-routing.module';
import { SunuKalpeHomeModule } from './home/home.module';
import { SunuKalpeEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    SunuKalpeSharedModule,
    SunuKalpeCoreModule,
    SunuKalpeHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    SunuKalpeEntityModule,
    SunuKalpeAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
})
export class SunuKalpeAppModule {}
