import { Moment } from 'moment';

export interface IDepot {
  id?: number;
  dateDepot?: Moment;
  montantDepot?: number;
}

export class Depot implements IDepot {
  constructor(public id?: number, public dateDepot?: Moment, public montantDepot?: number) {}
}
