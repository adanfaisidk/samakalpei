import { Moment } from 'moment';

export interface IRetrait {
  id?: number;
  dateRetrait?: Moment;
  montantRetrait?: number;
}

export class Retrait implements IRetrait {
  constructor(public id?: number, public dateRetrait?: Moment, public montantRetrait?: number) {}
}
