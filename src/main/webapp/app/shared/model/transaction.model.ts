import { Moment } from 'moment';
import { ICompte } from 'app/shared/model/compte.model';

export interface ITransaction {
  id?: number;
  date?: Moment;
  numer?: string;
  reference?: string;
  type?: string;
  frais?: number;
  montant?: number;
  statut?: boolean;
  compteTo?: ICompte;
  compteFrom?: ICompte;
}

export class Transaction implements ITransaction {
  constructor(
    public id?: number,
    public date?: Moment,
    public numer?: string,
    public reference?: string,
    public type?: string,
    public frais?: number,
    public montant?: number,
    public statut?: boolean,
    public compteTo?: ICompte,
    public compteFrom?: ICompte
  ) {
    this.statut = this.statut || false;
  }
}
