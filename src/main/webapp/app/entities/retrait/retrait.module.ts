import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SunuKalpeSharedModule } from 'app/shared/shared.module';
import { RetraitComponent } from './retrait.component';
import { RetraitDetailComponent } from './retrait-detail.component';
import { RetraitUpdateComponent } from './retrait-update.component';
import { RetraitDeleteDialogComponent } from './retrait-delete-dialog.component';
import { retraitRoute } from './retrait.route';

@NgModule({
  imports: [SunuKalpeSharedModule, RouterModule.forChild(retraitRoute)],
  declarations: [RetraitComponent, RetraitDetailComponent, RetraitUpdateComponent, RetraitDeleteDialogComponent],
  entryComponents: [RetraitDeleteDialogComponent],
})
export class SunuKalpeRetraitModule {}
