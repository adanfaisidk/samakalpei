import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRetrait } from 'app/shared/model/retrait.model';

@Component({
  selector: 'jhi-retrait-detail',
  templateUrl: './retrait-detail.component.html',
})
export class RetraitDetailComponent implements OnInit {
  retrait: IRetrait | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ retrait }) => (this.retrait = retrait));
  }

  previousState(): void {
    window.history.back();
  }
}
