import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IRetrait, Retrait } from 'app/shared/model/retrait.model';
import { RetraitService } from './retrait.service';

@Component({
  selector: 'jhi-retrait-update',
  templateUrl: './retrait-update.component.html',
})
export class RetraitUpdateComponent implements OnInit {
  isSaving = false;
  dateRetraitDp: any;

  editForm = this.fb.group({
    id: [],
    dateRetrait: [],
    montantRetrait: [],
  });

  constructor(protected retraitService: RetraitService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ retrait }) => {
      this.updateForm(retrait);
    });
  }

  updateForm(retrait: IRetrait): void {
    this.editForm.patchValue({
      id: retrait.id,
      dateRetrait: retrait.dateRetrait,
      montantRetrait: retrait.montantRetrait,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const retrait = this.createFromForm();
    if (retrait.id !== undefined) {
      this.subscribeToSaveResponse(this.retraitService.update(retrait));
    } else {
      this.subscribeToSaveResponse(this.retraitService.create(retrait));
    }
  }

  private createFromForm(): IRetrait {
    return {
      ...new Retrait(),
      id: this.editForm.get(['id'])!.value,
      dateRetrait: this.editForm.get(['dateRetrait'])!.value,
      montantRetrait: this.editForm.get(['montantRetrait'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRetrait>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
