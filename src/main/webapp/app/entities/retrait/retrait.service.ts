import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IRetrait } from 'app/shared/model/retrait.model';

type EntityResponseType = HttpResponse<IRetrait>;
type EntityArrayResponseType = HttpResponse<IRetrait[]>;

@Injectable({ providedIn: 'root' })
export class RetraitService {
  public resourceUrl = SERVER_API_URL + 'api/retraits';

  constructor(protected http: HttpClient) {}

  create(retrait: IRetrait): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(retrait);
    return this.http
      .post<IRetrait>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(retrait: IRetrait): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(retrait);
    return this.http
      .put<IRetrait>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IRetrait>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IRetrait[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(retrait: IRetrait): IRetrait {
    const copy: IRetrait = Object.assign({}, retrait, {
      dateRetrait: retrait.dateRetrait && retrait.dateRetrait.isValid() ? retrait.dateRetrait.format(DATE_FORMAT) : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateRetrait = res.body.dateRetrait ? moment(res.body.dateRetrait) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((retrait: IRetrait) => {
        retrait.dateRetrait = retrait.dateRetrait ? moment(retrait.dateRetrait) : undefined;
      });
    }
    return res;
  }
}
