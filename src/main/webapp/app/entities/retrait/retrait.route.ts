import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IRetrait, Retrait } from 'app/shared/model/retrait.model';
import { RetraitService } from './retrait.service';
import { RetraitComponent } from './retrait.component';
import { RetraitDetailComponent } from './retrait-detail.component';
import { RetraitUpdateComponent } from './retrait-update.component';

@Injectable({ providedIn: 'root' })
export class RetraitResolve implements Resolve<IRetrait> {
  constructor(private service: RetraitService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRetrait> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((retrait: HttpResponse<Retrait>) => {
          if (retrait.body) {
            return of(retrait.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Retrait());
  }
}

export const retraitRoute: Routes = [
  {
    path: '',
    component: RetraitComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'sunuKalpeApp.retrait.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: RetraitDetailComponent,
    resolve: {
      retrait: RetraitResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'sunuKalpeApp.retrait.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: RetraitUpdateComponent,
    resolve: {
      retrait: RetraitResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'sunuKalpeApp.retrait.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: RetraitUpdateComponent,
    resolve: {
      retrait: RetraitResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'sunuKalpeApp.retrait.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
