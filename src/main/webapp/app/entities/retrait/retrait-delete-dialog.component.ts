import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRetrait } from 'app/shared/model/retrait.model';
import { RetraitService } from './retrait.service';

@Component({
  templateUrl: './retrait-delete-dialog.component.html',
})
export class RetraitDeleteDialogComponent {
  retrait?: IRetrait;

  constructor(protected retraitService: RetraitService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.retraitService.delete(id).subscribe(() => {
      this.eventManager.broadcast('retraitListModification');
      this.activeModal.close();
    });
  }
}
