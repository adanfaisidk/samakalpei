import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'depot',
        loadChildren: () => import('./depot/depot.module').then(m => m.SunuKalpeDepotModule),
      },
      {
        path: 'retrait',
        loadChildren: () => import('./retrait/retrait.module').then(m => m.SunuKalpeRetraitModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class SunuKalpeEntityModule {}
