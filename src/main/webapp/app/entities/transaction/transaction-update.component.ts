import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITransaction, Transaction } from 'app/shared/model/transaction.model';
import { TransactionService } from './transaction.service';
import { ICompte } from 'app/shared/model/compte.model';
import { CompteService } from 'app/entities/compte/compte.service';

@Component({
  selector: 'jhi-transaction-update',
  templateUrl: './transaction-update.component.html',
})
export class TransactionUpdateComponent implements OnInit {
  isSaving = false;
  comptes: ICompte[] = [];
  dateDp: any;

  editForm = this.fb.group({
    id: [],
    date: [],
    numer: [],
    reference: [],
    type: [],
    frais: [],
    montant: [],
    statut: [],
    compteTo: [],
    compteFrom: [],
  });

  constructor(
    protected transactionService: TransactionService,
    protected compteService: CompteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ transaction }) => {
      this.updateForm(transaction);

      this.compteService.query().subscribe((res: HttpResponse<ICompte[]>) => (this.comptes = res.body || []));
    });
  }

  updateForm(transaction: ITransaction): void {
    this.editForm.patchValue({
      id: transaction.id,
      date: transaction.date,
      numer: transaction.numer,
      reference: transaction.reference,
      type: transaction.type,
      frais: transaction.frais,
      montant: transaction.montant,
      statut: transaction.statut,
      compteTo: transaction.compteTo,
      compteFrom: transaction.compteFrom,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const transaction = this.createFromForm();
    if (transaction.id !== undefined) {
      this.subscribeToSaveResponse(this.transactionService.update(transaction));
    } else {
      this.subscribeToSaveResponse(this.transactionService.create(transaction));
    }
  }

  private createFromForm(): ITransaction {
    return {
      ...new Transaction(),
      id: this.editForm.get(['id'])!.value,
      date: this.editForm.get(['date'])!.value,
      numer: this.editForm.get(['numer'])!.value,
      reference: this.editForm.get(['reference'])!.value,
      type: this.editForm.get(['type'])!.value,
      frais: this.editForm.get(['frais'])!.value,
      montant: this.editForm.get(['montant'])!.value,
      statut: this.editForm.get(['statut'])!.value,
      compteTo: this.editForm.get(['compteTo'])!.value,
      compteFrom: this.editForm.get(['compteFrom'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITransaction>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICompte): any {
    return item.id;
  }
}
